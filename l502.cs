using System;
using System.Runtime.InteropServices;
using System.Text;
using lpcieapi;

namespace x502api
{
    public class L502 : X502
    {       
        /************* Экспорт функций из библиотеки C l502api.dll *****************************/
        [DllImport("l502api.dll")]//, CallingConvention=CallingConvention.StdCall, CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern Int32 L502_GetSerialList(byte[,] serials, UInt32 size, GetDevsFlags flags, out UInt32 devcnt);
        [DllImport("l502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs L502_Open(IntPtr hnd, string serial);
        [DllImport("l502api.dll")]
        static extern Int32 L502_GetDevRecordsList(IntPtr list, UInt32 size,
                                                    GetDevsFlags flags, out UInt32 devcnt);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetDriverVersion(IntPtr hnd, out UInt32 ver);


        //получить список серийных номеров
        public static Int32 GetSerialList(out string[] serials, GetDevsFlags flags)
        {
            UInt32 devcnt;
            byte[,] ser_arr = null;
            /* получаем общее количество устройств в системе */
            Int32 res = L502_GetSerialList(ser_arr, 0, flags, out devcnt);
            if ((res >= 0) && (devcnt > 0))
            {
                /* выделяем массив под полученное кол-во серийных номеров */
                ser_arr = new byte[devcnt, SERIAL_SIZE];
                /* получаем серийные номера */
                res = L502_GetSerialList(ser_arr, devcnt, flags, out devcnt);
                if (res > 0)
                {
                    /* преобразуем серийные номера в строки */
                    serials = new string[res];
                    for (Int32 i = 0; i < res; i++)
                    {
                        int j;
                        char[] ser = new char[SERIAL_SIZE];
                        for (j = 0; (j < SERIAL_SIZE) && ser_arr[i, j] != '\0'; j++)
                        {
                            ser[j] = (char)ser_arr[i, j];
                        }
                        serials[i] = new string(ser, 0, j);
                    }
                }
                else
                {
                    serials = new string[0];
                }
            }
            else
            {
                serials = new string[0];
            }

            return res;
        }




        public static Int32 GetDevRecordsList(out DevRec[] devrec, GetDevsFlags flags)
        {
            UInt32 devcnt;
            IntPtr ptr = IntPtr.Zero;
            /* получаем общее количество устройств в системе */
            Int32 res = L502_GetDevRecordsList(ptr, 0, flags, out devcnt);
            if ((res >= 0) && (devcnt > 0))
            {
                int size = Marshal.SizeOf(typeof(DevRec.DevRecStruct));
                ptr = Marshal.AllocHGlobal(size * (int)devcnt);
                
                /* получаем серийные номера */
                res = L502_GetDevRecordsList(ptr, devcnt, flags, out devcnt);
                if (res > 0)
                {
                    /* преобразуем серийные номера в строки */
                    devrec = new DevRec[res];
                    for (Int32 i = 0; i < res; i++)
                    {
                        IntPtr recptr = new IntPtr(ptr.ToInt64() + size * i);
                        devrec[i] = new DevRec((DevRec.DevRecStruct)Marshal.PtrToStructure(recptr, typeof(DevRec.DevRecStruct)));
                    }
                }
                else
                {
                    devrec = new DevRec[0];
                }
                Marshal.FreeHGlobal(ptr);
            }
            else
            {
                devrec = new DevRec[0];
            }

            return res;
        }
        
        public lpcie.Errs Open(string serial) { return L502_Open(hnd, serial==null ? "" : serial); }

        public UInt32 DriverVersion
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = L502_GetDriverVersion(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        
        

        


        
        /************************* функции, оставленные для совместимости ************************/
        public enum DmaCh : uint
        {
            IN = X502.StreamCh.IN, /**< Общий канал DMA на ввод */
            OUT = X502.StreamCh.OUT  /**< Общий канал DMA на вывод */
        }

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDmaBufSize(IntPtr hnd, UInt32 dma_ch, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDmaIrqStep(IntPtr hnd, UInt32 dma_ch, UInt32 step);        
        [DllImport("l502api.dll", EntryPoint = "L502_GetDllVersion")]
        public static extern UInt32 GetDllVersion();

        public lpcie.Errs SetDmaBufSize(UInt32 dma_ch, UInt32 size)
        {
            return L502_SetDmaBufSize(hnd, dma_ch, size);
        }
        public lpcie.Errs SetDmaIrqStep(UInt32 dma_ch, UInt32 step)
        {
            return L502_SetDmaIrqStep(hnd, dma_ch, step);
        }      
    }
}