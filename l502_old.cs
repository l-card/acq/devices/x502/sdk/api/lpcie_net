﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using lpcieapi;

namespace l502api
{
    public class L502
    {
        /** Максимальное количество логических каналов в таблице*/
        public const UInt32 LTABLE_MAX_CH_CNT = 256;
        /** Количество диапазонов для измерения напряжений */
        public const uint ADC_RANGE_CNT = 6;
        /** Максимальное значение для аппаратного усреднения по логическому каналу */
        public const UInt32 LCH_AVG_SIZE_MAX = 128;
        /** Максимальное значения делителя частоты АЦП */
        public const UInt32 ADC_FREQ_DIV_MAX = (1024 * 1024);
        /** Максимальное значение делителя частоты синхронного цифрового ввода */
        public const UInt32 DIN_FREQ_DIV_MAX = (1024 * 1024);
        /** Максимальное значение межкадровой задержки для АЦП */
        public const UInt32 ADC_INTERFRAME_DELAY_MAX = (0x1FFFFF);
        /** Таймаут по умолчанию для выполнения команды к BlackFin*/
        public const UInt32 BF_CMD_DEFAULT_TOUT = 500;
        /** Код АЦП, соответствующий максимальному значению шкалы */
        public const UInt32 ADC_SCALE_CODE_MAX = 6000000;
        /** Код ЦАП, соответствующий максимальному значению шкалы */
        public const UInt32 DAC_SCALE_CODE_MAX = 30000;

        /** Максимальное количество символов в строке с названием устройства */
        public const UInt32 DEVNAME_SIZE = x502api.X502.DEVNAME_SIZE;
        /** Максимальное количество символов в строке с серийным номером */
        public const UInt32 SERIAL_SIZE = x502api.X502.SERIAL_SIZE;

        /** Размер пользовательской области Flash-памяти */
        public const UInt32 FLASH_USER_SIZE = 0x100000;

        /** Стандартный таймаут на выполнение запроса к BlackFin в мс */
        public const UInt32 BF_REQ_TOUT = 500;


        /** Диапазон ЦАП в вольтах */
        public const UInt32 DAC_RANGE = 5;

        /** Количество каналов ЦАП */
        public const UInt32 DAC_CH_CNT = 2;

        /** слово в потоке, означающее, что произошло переполнение */
        public const UInt32 STREAM_IN_MSG_OVERFLOW = 0x01010000;

        /** Максимально возможное значение внешней опорной частоты */
        public const UInt32 EXT_REF_FREQ_MAX = 200000;




        /** Флаги, управляющие поиском присутствующих модулей */
        [Flags]
        public enum GetDevsFlags : uint
        {
            /** Признак, что нужно вернуть серийные номера только тех устройств,
                которые еще не открыты */
            ONLY_NOT_OPENED = x502api.X502.GetDevsFlags.ONLY_NOT_OPENED
        }

        /** Флаги для управления цифровыми выходами */
        [Flags]
        public enum DigoutFlags : uint
        {
            WORD_DIS_H = 0x00020000, /**< Запрещение (перевод в третье состояние)
                                                 старшей половины цифровых выходов */
            WORD_DIS_L = 0x00010000  /**< Запрещение младшей половины
                                                      цифровых выходов */
        }

        /** Константы для выбора опорной частоты */
        public enum RefFreqVal : uint
        {
            FREQ_2000KHZ = 2000000, /**< Частота 2МГц */
            FREQ_1500KHZ = 1500000 /**< Частота 1.5МГц */
        }

        /** Диапазоны измерения для канала АЦП */
        public enum AdcRange : uint
        {
            RANGE_10 = 0, /**< Диапазон +/-10V */
            RANGE_5 = 1, /**< Диапазон +/-5V */
            RANGE_2 = 2, /**< Диапазон +/-2V */
            RANGE_1 = 3, /**< Диапазон +/-1V */
            RANGE_05 = 4, /**< Диапазон +/-0.5V */
            RANGE_02 = 5  /**< Диапазон +/-0.2V */
        }

        /** Режим измерения для логического канала */
        public enum LchMode : uint
        {
            COMM = 0, /**< Измерение напряжения относительно общей земли */
            DIFF = 1, /**< Дифференциальное измерение напряжения */
            ZERO = 2  /**< Измерение собственного нуля */
        }

        /** Режимы синхронизации */
        public enum Sync : uint
        {
            INTERNAL = 0, /**< Внутренний сигнал */
            EXTERNAL_MASTER = 1, /**< От внешнего мастера по разъему синхронизации */
            DI_SYN1_RISE = 2, /**< По фронту сигнала DI_SYN1 */
            DI_SYN2_RISE = 3, /**< По фронту сигнала DI_SYN2 */
            DI_SYN1_FALL = 6, /**< По спаду сигнала DI_SYN1 */
            DI_SYN2_FALL = 7  /**< По спаду сигнала DI_SYN2 */
        }

        /** Флаги, управляющие обработкой принятых данных */
        [Flags]
        public enum ProcFlags : uint
        {
            VOLT = 1 /**< Признак, что нужно преобразовать значения
                                            АЦП в вольты */
        }


        /** Флаги для обозначения синхронных потоков данных */
        [Flags]
        public enum Streams : uint
        {
            ADC = 0x01, /**< Поток данных от АЦП */
            DIN = 0x02, /**< Поток данных с цифровых входов */
            DAC1 = 0x10, /**< Поток данных первого канала ЦАП */
            DAC2 = 0x20, /**< Поток данных второго канала ЦАП */
            DOUT = 0x40, /**< Поток данных на цифровые выводы */
            /** Объединение всех флагов, обозначающих потоки данных на ввод */
            ALL_IN = ADC | DIN,
            /** Объединение всех флагов, обозначающих потоки данных на вывод */
            ALL_OUT = DAC1 | DAC2 | DOUT
        }

        /** Константы, определяющие тип передаваемого отсчета из ПК в модуль */
        public enum StreamOutWordType : uint
        {
            DOUT = 0x0, /**< Цифровой вывод */
            DAC1 = 0x40000000, /**< Код для 1-го канала ЦАП */
            DAC2 = 0x80000000  /**< Код для 2-го канала ЦАП */
        };

        /** Режим работы модуля L502 */
        public enum Mode : uint
        {
            FPGA = 0, /**< Все потоки данных передаются через ПЛИС минуя
                              сигнальный процессор BlackFin */
            DSP = 1, /**< Все потоки данных передаются через сигнальный
                              процессор, который должен быть загружен
                              прошивкой для обработки этих потоков */
            DEBUG = 2  /**< Отладочный режим */
        }

        /** Номера каналов ЦАП. */
        public enum DacCh : uint
        {
            CH1 = 0, /**< Первый канал ЦАП */
            CH2 = 1  /**< Второй канал ЦАП */
        }

        /** Флаги, используемые при выводе данных на ЦАП. */
        [Flags]
        public enum DacOutFlags : uint
        {
            /** Указывает, что значение задано в Вольтах и при выводе его нужно
                перевести его в коды ЦАП. Если флаг не указан, то считается, что значение
                изначально в кодах */
            VOLT = 0x0001,
            /** Указывает, что нужно применить калибровочные коэффициенты перед
                выводом значения на ЦАП. */
            CALIBR = 0x0002
        }


        /** Номера каналов DMA */
        public enum DmaCh : uint
        {
            IN = 0, /**< Общий канал DMA на ввод */
            OUT = 1  /**< Общий канал DMA на вывод */
        }


        /**  Цифровые линии, на которых можно включить подтягивающие резисторы */
        [Flags]
        public enum Pullups : uint
        {
            DI_H = 0x01, /**< Старшая половина цифровых входов */
            DI_L = 0x02, /**< Младшая половина цифровых входов */
            DI_SYN1 = 0x04, /**< Линия SYN1 */
            DI_SYN2 = 0x08  /**< Линия SYN2 */
        }


        /** Флаги, определяющие наличие опций в модуле */
        [Flags]
        public enum DevFlags : uint
        {
            /** Признак наличия двухканального канального ЦАП */
            DAC_PRESENT = x502api.X502.DevFlags.DAC_PRESENT,
            /** Признак наличия гальваноразвязки */
            GAL_PRESENT = x502api.X502.DevFlags.GAL_PRESENT,
            /** Признак наличия сигнального процессора BlackFin */
            BF_PRESENT = x502api.X502.DevFlags.BF_PRESENT,
            /** Признак, что во Flash-памяти присутствует информация о модуле */
            FLASH_DATA_VALID = 0x00010000,
            /** Признак, что во Flash-памяти присутствуют действительные калибровочные
                коэффициенты АЦП */
            FLASH_ADC_CALIBR_VALID = 0x00020000,
            /** Признак, что во Flash-памяти присутствуют действительные калибровочные
                коэффициенты ЦАП */
            FLASH_DAC_CALIBR_VALID = 0x00040000
        }

        [Flags]
        public enum OutCycleFlags : uint
        {
            FORCE = 0x01
        }


        /** Калибровочные коэффициенты диапазона. */
        [StructLayout(LayoutKind.Sequential)]
        public struct CbrCoef
        {
            double _offs; /**< смещение нуля */
            double _k; /**< коэффициент шкалы */
            public double Offs { get { return _offs; } }
            public double k { get { return _k; } }
        };


        /**  Калибровочные коэффициенты модуля. */
        [StructLayout(LayoutKind.Sequential)]
        public struct Cbr
        {
            /** Калибровочные коэффициенты АЦП */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)ADC_RANGE_CNT)]
            CbrCoef[] adc;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            UInt32[] res1; /**< Резерв */
            /** Калибровочные коэффициенты ЦАП */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)DAC_CH_CNT)]
            CbrCoef[] dac;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            UInt32[] res2; /**< Резерв */

            public CbrCoef[] Adc { get { return adc; } }
            public CbrCoef[] Dac { get { return dac; } }
        };

        /** Информация о модуле L502. */
        [StructLayout(LayoutKind.Sequential)]
        public struct Info
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)DEVNAME_SIZE)]
            char[] _name; /**< Название устройства ("L502") */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)SERIAL_SIZE)]
            char[] _serial; /**< Серийный номер */
            DevFlags devflags; /**< Флаги из #t_dev_flags, описывающие наличие
                            в модуле определенных опций */
            UInt16 fpga_ver; /**< Версия ПЛИС (старший байт - мажорная, младший - минорная) */
            Byte plda_ver; /**< Версия ПЛИС, управляющего аналоговой частью */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 121)]
            Byte[] res; /**< Резерв */
            Cbr cbr; /**< Заводские калибровочные коэффициенты (из Flash-памяти) */

            public string Name { get { return new string(_name); } }
            public string Serial { get { return new string(_serial); } }
            public DevFlags DevFlags { get { return devflags; } }
            public UInt16 FpgaVer { get { return fpga_ver; } }
            public Byte PldaVer { get { return plda_ver; } }

            public string FpgaVerString { get { return String.Format("{0}.{1}", (fpga_ver >> 8) & 0xFF, fpga_ver & 0xFF); } }
            public Cbr Cbr { get { return cbr; } }
        };


        /* исключение при неверных параметрах */
        public class Exception : System.ApplicationException
        {
            lpcie.Errs m_err;
            public lpcie.Errs ErrorCode { get { return m_err; } }

            public Exception(lpcie.Errs err)
            {
                m_err = err;
            }

            public override string ToString()
            {
                return GetErrorString(m_err);
            }
        }

        /* класс - описание логического канала */
        public class LChannel
        {
            UInt32 m_avg;
            UInt32 m_phy_ch;
            LchMode m_mode;
            AdcRange m_range;

            public LChannel(UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg)
            {
                m_phy_ch = phy_ch;
                m_mode = mode;
                m_range = range;
                m_avg = avg;
            }

            public LchMode Mode { get { return m_mode; } }
            public AdcRange Range { get { return m_range; } }
            public UInt32 PhyCh { get { return m_phy_ch; } }
            public UInt32 Avg { get { return m_avg; } }
        }


        /************* Экспорт функций из библиотеки C l502api.dll *****************************/


        [DllImport("l502api.dll")]
        static extern IntPtr L502_Create();
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_Free(IntPtr hnd);
        [DllImport("l502api.dll")]//, CallingConvention=CallingConvention.StdCall, CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern Int32 L502_GetSerialList(byte[,] serials, UInt32 size, GetDevsFlags flags, out UInt32 devcnt);
        [DllImport("l502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs L502_Open(IntPtr hnd, string serial);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_Close(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetDevInfo(IntPtr hnd, out Info info);

        /* Функции для изменения настроек модуля */
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_Configure(IntPtr hnd, UInt32 flags);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetLChannel(IntPtr hnd, UInt32 lch, UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetLChannelCount(IntPtr hnd, UInt32 lch_cnt);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetLChannelCount(IntPtr hnd, out UInt32 lch_cnt);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetAdcFreqDivider(IntPtr hnd, UInt32 adc_freq_div);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetAdcInterframeDelay(IntPtr hnd, UInt32 delay);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDinFreqDivider(IntPtr hnd, UInt32 din_freq_div);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetAdcFreq(IntPtr hnd, ref double f_adc, ref double f_frame);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDinFreq(IntPtr hnd, ref double f_din);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetAdcFreq(IntPtr hnd, out double f_adc, out double f_frame);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetRefFreq(IntPtr hnd, RefFreqVal freq);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetSyncMode(IntPtr hnd, Sync sync_mode);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetSyncStartMode(IntPtr hnd, Sync sync_start_mode);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetMode(IntPtr hnd, Mode mode);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetMode(IntPtr hnd, out Mode mode);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetAdcCoef(IntPtr hnd, AdcRange range, double k, double offs);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetAdcCoef(IntPtr hnd, AdcRange range, out double k, out double offs);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDacCoef(IntPtr hnd, DacCh ch, double k, double offs);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetDacCoef(IntPtr hnd, DacCh ch, out double k, out double offs);
        /* Функции асинхронного ввода-вывода */
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_AsyncOutDac(IntPtr hnd, DacCh ch, double data, DacOutFlags flags);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_AsyncOutDig(IntPtr hnd, UInt32 val, UInt32 msk);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_AsyncInDig(IntPtr hnd, out UInt32 din);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_AsyncGetAdcFrame(IntPtr hnd, ProcFlags flags, UInt32 tout, double[] data);
        /* Функции для работы с синхронным потоковым вводом-выводом */
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_StreamsEnable(IntPtr hnd, Streams streams);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_StreamsDisable(IntPtr hnd, Streams streams);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_StreamsStart(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_IsRunning(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_StreamsStop(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern Int32 L502_Recv(IntPtr hnd, UInt32[] buf, UInt32 size, UInt32 tout);
        [DllImport("l502api.dll")]
        static extern Int32 L502_Send(IntPtr hnd, UInt32[] buf, UInt32 size, UInt32 tout);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_ProcessAdcData(IntPtr hnd, UInt32[] src, double[] dest, ref UInt32 size, ProcFlags flags);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_ProcessData(IntPtr hnd, UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_ProcessDataWithUserExt(IntPtr hnd, UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size,
            UInt32[] usr_data, ref UInt32 usr_data_size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_PrepareData(IntPtr hnd, double[] dac1, double[] dac2, UInt32[] digout,
            UInt32 size, DacOutFlags flags, UInt32[] out_buf);

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetRecvReadyCount(IntPtr hnd, out UInt32 rdy_cnt);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetSendReadyCount(IntPtr hnd, out UInt32 rdy_cnt);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetNextExpectedLchNum(IntPtr hnd, out UInt32 lch);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_PreloadStart(IntPtr hnd);

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_OutCycleLoadStart(IntPtr hnd, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_OutCycleSetup(IntPtr hnd, OutCycleFlags flags);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_OutCycleStop(IntPtr hnd, OutCycleFlags flags);

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDmaBufSize(IntPtr hnd, UInt32 dma_ch, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDmaIrqStep(IntPtr hnd, UInt32 dma_ch, UInt32 step);

        //Функции для работы с сигнальным процессором
        [DllImport("l502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs L502_BfLoadFirmware(IntPtr hnd, string filename);

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_BfCheckFirmwareIsLoaded(IntPtr hnd, out UInt32 version);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_BfMemRead(IntPtr hnd, UInt32 addr, UInt32[] regs, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_BfMemWrite(IntPtr hnd, UInt32 addr, UInt32[] regs, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_BfExecCmd(IntPtr hnd, UInt16 cmd_code, UInt32 par,
            UInt32[] snd_data, UInt32 snd_size, UInt32[] rcv_data, UInt32 rcv_size, UInt32 tout, out UInt32 recvd_size);

        //Функции для работы с Flash-памятью модуля
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_FlashRead(IntPtr hnd, UInt32 addr, byte[] data, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_FlashWrite(IntPtr hnd, UInt32 addr, byte[] data, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_FlashErase(IntPtr hnd, UInt32 addr, UInt32 size);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_FlashWriteEnable(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_FlashWriteDisable(IntPtr hnd);

        //Дополнительные вспомогательные функции
        [DllImport("l502api.dll", EntryPoint = "L502_GetDllVersion")]
        public static extern UInt32 GetDllVersion();
        [DllImport("l502api.dll", CharSet = CharSet.Ansi)]
        static extern IntPtr L502_GetErrorString(lpcie.Errs err);

        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_GetDriverVersion(IntPtr hnd, out UInt32 ver);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_LedBlink(IntPtr hnd);
        [DllImport("l502api.dll")]
        static extern lpcie.Errs L502_SetDigInPullup(IntPtr hnd, Pullups pullups);


        //получить список серийных номеров
        public static Int32 GetSerialList(out string[] serials, GetDevsFlags flags)
        {
            UInt32 devcnt;
            byte[,] ser_arr = null;
            /* получаем общее количество устройств в системе */
            Int32 res = L502_GetSerialList(ser_arr, 0, flags, out devcnt);
            if ((res >= 0) && (devcnt > 0))
            {
                /* выделяем массив под полученное кол-во серийных номеров */
                ser_arr = new byte[devcnt, SERIAL_SIZE];
                /* получаем серийные номера */
                res = L502_GetSerialList(ser_arr, devcnt, flags, out devcnt);
                if (res > 0)
                {
                    /* преобразуем серийные номера в строки */
                    serials = new string[res];
                    for (Int32 i = 0; i < res; i++)
                    {
                        int j;
                        char[] ser = new char[SERIAL_SIZE];
                        for (j = 0; (j < SERIAL_SIZE) && ser_arr[i, j] != '\0'; j++)
                        {
                            ser[j] = (char)ser_arr[i, j];
                        }
                        serials[i] = new string(ser, 0, j);
                    }
                }
                else
                    serials = new string[0];
            }
            else
                serials = new string[0];

            return res;
        }


        public L502()
        {
            hnd = L502_Create();
        }

        ~L502()
        {
            L502_Free(hnd);
        }

        public lpcie.Errs Open(string serial) { return L502_Open(hnd, serial == null ? "" : serial); }
        public lpcie.Errs Close() { return L502_Close(hnd); }

        public Info DevInfo
        {
            get
            {
                Info info;
                lpcie.Errs res = L502_GetDevInfo(hnd, out info);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return info;
            }
        }

        public lpcie.Errs Configure(UInt32 flags)
        {
            return L502_Configure(hnd, flags);
        }

        public lpcie.Errs SetLChannel(UInt32 lch, UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg)
        {
            return L502_SetLChannel(hnd, lch, phy_ch, mode, range, avg);
        }

        public LChannel[] LChannelTable
        {
            set
            {
                for (UInt32 i = 0; (i < value.Length) && (i < LTABLE_MAX_CH_CNT); i++)
                {
                    lpcie.Errs res = SetLChannel(i, value[i].PhyCh, value[i].Mode, value[i].Range, value[i].Avg);
                    if (res != lpcie.Errs.OK)
                        throw new Exception(res);
                }
            }
        }

        public UInt32 LChannelCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs res = L502_GetLChannelCount(hnd, out val);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return val;
            }
            set
            {
                lpcie.Errs res = L502_SetLChannelCount(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 AdcFreqDivider
        {
            set
            {
                lpcie.Errs res = L502_SetAdcFreqDivider(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 AdcInterframeDelay
        {
            set
            {
                lpcie.Errs res = L502_SetAdcInterframeDelay(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 DinFreqDivider
        {
            set
            {
                lpcie.Errs res = L502_SetDinFreqDivider(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public lpcie.Errs SetAdcFreq(ref double f_adc, ref double f_frame)
        {
            return L502_SetAdcFreq(hnd, ref f_adc, ref f_frame);
        }

        public lpcie.Errs SetDinFreq(ref double f_din)
        {
            return L502_SetDinFreq(hnd, ref f_din);
        }

        public lpcie.Errs GetAdcFreq(out double f_adc, out double f_frame)
        {
            return L502_GetAdcFreq(hnd, out f_adc, out f_frame);
        }

        public RefFreqVal RefFreq
        {
            set
            {
                lpcie.Errs res = L502_SetRefFreq(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public Sync SyncMode
        {
            set
            {
                lpcie.Errs res = L502_SetSyncMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public Sync SyncStartMode
        {
            set
            {
                lpcie.Errs res = L502_SetSyncStartMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public Mode ModuleMode
        {
            get
            {
                Mode mode;
                lpcie.Errs res = L502_GetMode(hnd, out mode);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return mode;

            }
            set
            {
                lpcie.Errs res = L502_SetMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }


        public lpcie.Errs SetAdcCoef(AdcRange range, double k, double offs)
        {
            return L502_SetAdcCoef(hnd, range, k, offs);
        }

        public lpcie.Errs GetAdcCoef(AdcRange range, out double k, out double offs)
        {
            return L502_GetAdcCoef(hnd, range, out k, out offs);
        }

        public lpcie.Errs SetDacCoef(DacCh ch, double k, double offs)
        {
            return L502_SetDacCoef(hnd, ch, k, offs);
        }

        public lpcie.Errs GetDacCoef(DacCh ch, out double k, out double offs)
        {
            return L502_GetDacCoef(hnd, ch, out k, out offs);
        }


        public lpcie.Errs AsyncOutDac(DacCh ch, double data, DacOutFlags flags)
        {
            return L502_AsyncOutDac(hnd, ch, data, flags);
        }
        public lpcie.Errs AsyncOutDig(UInt32 val, UInt32 msk)
        {
            return L502_AsyncOutDig(hnd, val, msk);
        }
        public lpcie.Errs AsyncInDig(out UInt32 din)
        {
            return L502_AsyncInDig(hnd, out din);
        }
        public lpcie.Errs AsyncGetAdcFrame(ProcFlags flags, UInt32 tout, double[] data)
        {
            return data.Length < LChannelCount ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                   L502_AsyncGetAdcFrame(hnd, flags, tout, data);
        }
        public lpcie.Errs StreamsEnable(Streams streams)
        {
            return L502_StreamsEnable(hnd, streams);
        }
        public lpcie.Errs StreamsDisable(Streams streams)
        {
            return L502_StreamsDisable(hnd, streams);
        }
        public lpcie.Errs StreamsStart()
        {
            return L502_StreamsStart(hnd);
        }
        public lpcie.Errs StreamsStop()
        {
            return L502_StreamsStop(hnd);
        }

        public Int32 Recv(UInt32[] buf, UInt32 size, UInt32 tout)
        {
            return buf.Length < size ? (Int32)lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_Recv(hnd, buf, size, tout);
        }

        public Int32 Send(UInt32[] buf, UInt32 size, UInt32 tout)
        {
            return buf.Length < size ? (Int32)lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_Send(hnd, buf, size, tout);
        }

        public lpcie.Errs ProcessAdcData(UInt32[] src, double[] dest, ref UInt32 size, ProcFlags flags)
        {
            return (src.Length < size) || (dest.Length < size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_ProcessAdcData(hnd, src, dest, ref size, flags);
        }

        public lpcie.Errs ProcessData(UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size)
        {
            return (src.Length < size) || (adc_data.Length < adc_data_size) || (din_data.Length < din_data_size) ?
                lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_ProcessData(hnd, src, size, flags, adc_data, ref adc_data_size, din_data, ref din_data_size);
        }

        public lpcie.Errs ProcessDataWithUserExt(UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size,
            UInt32[] usr_data, ref UInt32 usr_data_size)
        {
            return (src.Length < size) || (adc_data.Length < adc_data_size) || (din_data.Length < din_data_size)
                || (usr_data.Length < usr_data_size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
               L502_ProcessDataWithUserExt(hnd, src, size, flags,
                adc_data, ref adc_data_size, din_data, ref din_data_size,
                usr_data, ref usr_data_size);
        }

        public lpcie.Errs PrepareData(double[] dac1, double[] dac2, UInt32[] digout,
            UInt32 size, DacOutFlags flags, UInt32[] out_buf)
        {
            return L502_PrepareData(hnd, dac1, dac2, digout, size, flags, out_buf);
        }

        public UInt32 RecvReadyCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = L502_GetRecvReadyCount(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public UInt32 SendReadyCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = L502_GetSendReadyCount(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public UInt32 NextExpectedLchNum
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = L502_GetNextExpectedLchNum(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public lpcie.Errs PreloadStart()
        {
            return L502_PreloadStart(hnd);
        }

        public lpcie.Errs OutCycleLoadStart(UInt32 size)
        {
            return L502_OutCycleLoadStart(hnd, size);
        }

        public lpcie.Errs OutCycleSetup(OutCycleFlags flags)
        {
            return L502_OutCycleSetup(hnd, flags);
        }

        public lpcie.Errs OutCycleStop(OutCycleFlags flags)
        {
            return L502_OutCycleStop(hnd, flags);
        }

        public lpcie.Errs SetDmaBufSize(UInt32 dma_ch, UInt32 size)
        {
            return L502_SetDmaBufSize(hnd, dma_ch, size);
        }
        public lpcie.Errs SetDmaIrqStep(UInt32 dma_ch, UInt32 step)
        {
            return L502_SetDmaIrqStep(hnd, dma_ch, step);
        }

        public lpcie.Errs BfLoadFirmware(string filename)
        {
            return L502_BfLoadFirmware(hnd, filename);
        }
        public lpcie.Errs BfCheckFirmwareIsLoaded(out UInt32 version)
        {
            return L502_BfCheckFirmwareIsLoaded(hnd, out version);
        }
        public lpcie.Errs BfMemRead(UInt32 addr, UInt32[] regs, UInt32 size)
        {
            return regs.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_BfMemRead(hnd, addr, regs, size);
        }
        public lpcie.Errs BfMemWrite(UInt32 addr, UInt32[] regs, UInt32 size)
        {
            return regs.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_BfMemWrite(hnd, addr, regs, size);
        }
        public lpcie.Errs BfExecCmd(UInt16 cmd_code, UInt32 par,
            UInt32[] snd_data, UInt32 snd_size, UInt32[] rcv_data, UInt32 rcv_size, UInt32 tout, out UInt32 recvd_size)
        {
            recvd_size = 0;
            return (snd_data.Length < snd_size) || (rcv_data.Length < rcv_size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_BfExecCmd(hnd, cmd_code, par, snd_data, snd_size, rcv_data, rcv_size, tout, out recvd_size);
        }

        public lpcie.Errs FlashRead(UInt32 addr, byte[] data, UInt32 size)
        {
            return data.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_FlashRead(hnd, addr, data, size);
        }
        public lpcie.Errs FlashWrite(UInt32 addr, byte[] data, UInt32 size)
        {
            return data.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                L502_FlashWrite(hnd, addr, data, size);
        }

        public lpcie.Errs FlashErase(UInt32 addr, UInt32 size)
        {
            return L502_FlashErase(hnd, addr, size);
        }
        public lpcie.Errs FlashWriteEnable()
        {
            return L502_FlashWriteEnable(hnd);
        }
        public lpcie.Errs FlashWriteDisable()
        {
            return L502_FlashWriteDisable(hnd);
        }

        public bool IsRunning()
        {
            return L502_IsRunning(hnd) == lpcie.Errs.OK;
        }
        public UInt32 DriverVersion
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = L502_GetDriverVersion(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }
        public lpcie.Errs LedBlink()
        {
            return L502_LedBlink(hnd);
        }

        public Pullups DigInPullup
        {
            set
            {
                lpcie.Errs err = L502_SetDigInPullup(hnd, value);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
            }
        }

        public static string GetErrorString(lpcie.Errs err)
        {
            IntPtr ptr = L502_GetErrorString(err);
            string str = Marshal.PtrToStringAnsi(ptr);
            Encoding srcEncodingFormat = Encoding.GetEncoding("windows-1251");
            Encoding dstEncodingFormat = Encoding.UTF8;
            return dstEncodingFormat.GetString(Encoding.Convert(srcEncodingFormat, dstEncodingFormat, srcEncodingFormat.GetBytes(str)));
        }



        IntPtr hnd; /* описатель модуля */
    }
}
