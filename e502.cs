﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using lpcieapi;

namespace x502api
{
    public class E502 : X502
    {
        [DllImport("e502api.dll")]
        static extern Int32 E502_UsbGetSerialList(byte[,] serials, UInt32 size, GetDevsFlags flags, out UInt32 devcnt);
        [DllImport("e502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs E502_OpenUsb(IntPtr hnd, string serial);       
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_OpenByIpAddr(IntPtr hnd, UInt32 ip_addr, UInt32 flags, UInt32 tout);
        [DllImport("e502api.dll")]
        static extern Int32 E502_UsbGetDevRecordsList(IntPtr list, UInt32 size, GetDevsFlags flags, out UInt32 devcnt);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_MakeDevRecordByIpAddr(out X502.DevRec.DevRecStruct devrec, UInt32 ip_addr,
                                                            UInt32 flags, UInt32 tout);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_MakeDevRecordByEthSvc(out X502.DevRec.DevRecStruct devrec, IntPtr svc,
                                                            UInt32 flags, UInt32 tout);

        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_GetIpAddr(IntPtr hnd, out UInt32 ip_addr);
        [DllImport("e502api.dll")]
        static extern IntPtr E502_EthConfigCreate();
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigFree(IntPtr cfg);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigRead(IntPtr hnd, IntPtr cfg);
        [DllImport("e502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs E502_EthConfigWrite(IntPtr hnd, IntPtr cfg, string passwd);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigCopy(IntPtr src_cfg, IntPtr dst_cfg);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetEnabled(IntPtr cfg, out UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetEnabled(IntPtr cfg, UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetAutoIPEnabled(IntPtr cfg, out UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetAutoIPEnabled(IntPtr cfg, UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetUserMACEnabled(IntPtr cfg, out UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetUserMACEnabled(IntPtr cfg, UInt32 en);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetIPv4Addr(IntPtr cfg, out UInt32 ip_addr);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetIPv4Addr(IntPtr cfg, UInt32 ip_addr);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetIPv4Mask(IntPtr cfg, out UInt32 mask);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetIPv4Mask(IntPtr cfg, UInt32 mask);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetIPv4Gate(IntPtr cfg, out UInt32 gate);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetIPv4Gate(IntPtr cfg, UInt32 gate);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetUserMac(IntPtr cfg, byte[] mac);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetUserMac(IntPtr cfg, byte[] mac);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetFactoryMac(IntPtr cfg, byte[] mac);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigGetInstanceName(IntPtr cfg, byte[] name);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthConfigSetInstanceName(IntPtr cfg, string name);
        [DllImport("e502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs E502_EthConfigSetNewPassword(IntPtr cfg, string new_passwd);

        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_SwitchToBootloader(IntPtr hnd);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_ReloadFPGA(IntPtr hnd);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_CortexExecCmd(IntPtr hnd, UInt32 cmd_code, UInt32 par,
                                                    byte[] snd_data, UInt32 snd_size,
                                                    byte[] rcv_data, UInt32 rcv_size,
                                                    UInt32 tout, out UInt32 recvd_size);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcBrowseStart(out IntPtr pcontext, UInt32 flags);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcBrowseGetEvent(IntPtr context, out IntPtr svc, out EthSvcEvent svc_event, out UInt32 flags, UInt32 tout);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcBrowseStop(IntPtr context);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcRecordFree(IntPtr svc);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcRecordGetInstanceName(IntPtr svc, byte[] name);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcRecordGetDevSerial(IntPtr svc, byte[] serial);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcRecordResolveIPv4Addr(IntPtr svc, out UInt32 addr, UInt32 tout);
        [DllImport("e502api.dll")]
        static extern lpcie.Errs E502_EthSvcRecordIsSameInstance(IntPtr svc1, IntPtr svc2);


        static uint ipAddrToUint(IPAddress addr)
        {
            byte[] bytes = addr.GetAddressBytes();
            Array.Reverse(bytes); // flip big-endian(network order) to little-endian
            return BitConverter.ToUInt32(bytes, 0);
        }

        static IPAddress uintToIpAddr(uint intAddr)
        {
            byte[] bytes = BitConverter.GetBytes(intAddr);
            Array.Reverse(bytes);
            return new IPAddress(bytes);
        }


        public enum EthSvcEvent : uint {
            NONE     = 0, /**< Ни одного события не произошло */
            ADD      = 1, /**< Обнаружено появление нового сетевого сервиса */
            REMOVE   = 2, /**< Обнаружено исчезновение ранее доступного сетевого сервиса */
            CHANGED  = 3  /**< Изменение параметров ранее обнаруженного сетевого сервиса */
        }

        //получить список серийных номеров
        public static Int32 UsbGetSerialList(out string[] serials, GetDevsFlags flags)
        {
            UInt32 devcnt;
            byte[,] ser_arr = null;
            /* получаем общее количество устройств в системе */
            Int32 res = E502_UsbGetSerialList(ser_arr, 0, flags, out devcnt);
            if ((res >= 0) && (devcnt > 0))
            {
                /* выделяем массив под полученное кол-во серийных номеров */
                ser_arr = new byte[devcnt, SERIAL_SIZE];
                /* получаем серийные номера */
                res = E502_UsbGetSerialList(ser_arr, devcnt, flags, out devcnt);
                if (res > 0)
                {
                    /* преобразуем серийные номера в строки */
                    serials = new string[res];
                    for (Int32 i = 0; i < res; i++)
                    {
                        int j;
                        char[] ser = new char[SERIAL_SIZE];
                        for (j = 0; (j < SERIAL_SIZE) && ser_arr[i, j] != '\0'; j++)
                        {
                            ser[j] = (char)ser_arr[i, j];
                        }
                        serials[i] = new string(ser, 0, j);
                    }
                }
                else
                {
                    serials = new string[0];
                }
            }
            else
            {
                serials = new string[0];
            }

            return res;
        }




        public static Int32 UsbGetDevRecordsList(out DevRec[] devrec, GetDevsFlags flags)
        {
            UInt32 devcnt;
            IntPtr ptr = IntPtr.Zero;
            /* получаем общее количество устройств в системе */
            Int32 res = E502_UsbGetDevRecordsList(ptr, 0, flags, out devcnt);
            if ((res >= 0) && (devcnt > 0))
            {
                int size = Marshal.SizeOf(typeof(DevRec.DevRecStruct));
                ptr = Marshal.AllocHGlobal(size * (int)devcnt);

                /* получаем серийные номера */
                res = E502_UsbGetDevRecordsList(ptr, devcnt, flags, out devcnt);
                if (res > 0)
                {
                    /* преобразуем серийные номера в строки */
                    devrec = new DevRec[res];
                    for (Int32 i = 0; i < res; i++)
                    {
                        IntPtr recptr = new IntPtr(ptr.ToInt64() + size * i);
                        devrec[i] = new DevRec((DevRec.DevRecStruct)Marshal.PtrToStructure(recptr, typeof(DevRec.DevRecStruct)));
                    }
                }
                else
                {
                    devrec = new DevRec[0];
                }
                Marshal.FreeHGlobal(ptr);
            }
            else
            {
                devrec = new DevRec[0];
            }

            return res;
        }

        


        public lpcie.Errs OpenUsb(string serial) { return E502_OpenUsb(hnd, serial == null ? "" : serial); }
        public lpcie.Errs OpenByIpAddr(IPAddress ip_addr, UInt32 flags, UInt32 tout)
        {
            return E502_OpenByIpAddr(hnd, ipAddrToUint(ip_addr), flags, tout);
        }

        public lpcie.Errs OpenByIpAddr(string ip_addr_str, UInt32 flags, UInt32 tout)
        {
            return OpenByIpAddr(IPAddress.Parse(ip_addr_str), flags, tout);
        }

        static public X502.DevRec MakeDevRecordByIpAddr(IPAddress ip_addr, UInt32 flags, UInt32 tout)
        {
            DevRec.DevRecStruct devstruct;
            lpcie.Errs err = E502_MakeDevRecordByIpAddr(out devstruct, ipAddrToUint(ip_addr), flags, tout);
            if (err != lpcie.Errs.OK)
                return null;
            X502.DevRec rec = new X502.DevRec(devstruct);
            return rec;
        }



        static public X502.DevRec MakeDevRecordByIpAddr(string ip_addr_str, UInt32 flags, UInt32 tout)
        {
            return MakeDevRecordByIpAddr(IPAddress.Parse(ip_addr_str), flags, tout); 
        }

        public IPAddress IpAddr
        {
            get
            {
                UInt32 intAddr;
                lpcie.Errs err = E502_GetIpAddr(hnd, out intAddr);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return uintToIpAddr(intAddr);
            }
        }

        public lpcie.Errs SwitchToBootloader()
        {
            return E502_SwitchToBootloader(hnd);
        }

        public lpcie.Errs ReloadFPGA()
        {
            return E502_ReloadFPGA(hnd);
        }

        public lpcie.Errs CortexExecCmd(IntPtr hnd, UInt32 cmd_code, UInt32 par,
                                        byte[] snd_data, UInt32 snd_size,
                                        byte[] rcv_data, UInt32 rcv_size,
                                        UInt32 tout, out UInt32 recvd_size)
        {
            recvd_size = 0;
            return (snd_data.Length < snd_size) || (rcv_data.Length < rcv_size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                E502_CortexExecCmd(hnd, cmd_code, par, snd_data, snd_size, rcv_data, rcv_size, tout, out recvd_size);
        }


        public lpcie.Errs EthConfigRead(EthConfig cfg)
        {
            return E502_EthConfigRead(hnd, cfg.cfg_hnd);
        }


        public lpcie.Errs EthConfigWrite(EthConfig cfg, string passwd)
        {
            return E502_EthConfigWrite(hnd, cfg.cfg_hnd, passwd);
        }




        public class EthConfig
        {
            public IntPtr cfg_hnd;
            public EthConfig() { cfg_hnd = E502_EthConfigCreate(); }
            ~EthConfig() { E502_EthConfigFree(cfg_hnd); }

            public lpcie.Errs CopyTo(EthConfig cfg)
            {
                return E502_EthConfigCopy(cfg_hnd, cfg.cfg_hnd);
            }


            public bool Enabled
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetEnabled(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return val != 0;
                }
                set
                {
                    UInt32 val = value ? 1U : 0U;
                    lpcie.Errs err = E502_EthConfigSetEnabled(cfg_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public bool AutoIPEnabled
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetAutoIPEnabled(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return val != 0;
                }
                set
                {
                    UInt32 val = value ? 1U : 0U;
                    lpcie.Errs err = E502_EthConfigSetAutoIPEnabled(cfg_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public bool UserMACEnabled
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetUserMACEnabled(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return val != 0;
                }
                set
                {
                    UInt32 val = value ? 1U : 0U;
                    lpcie.Errs err = E502_EthConfigSetUserMACEnabled(cfg_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public IPAddress IPv4Addr
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetIPv4Addr(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return uintToIpAddr(val);
                }
                set
                {
                    lpcie.Errs err = E502_EthConfigSetIPv4Addr(cfg_hnd, ipAddrToUint(value));
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public IPAddress IPv4Mask
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetIPv4Mask(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return uintToIpAddr(val);
                }
                set
                {
                    lpcie.Errs err = E502_EthConfigSetIPv4Mask(cfg_hnd, ipAddrToUint(value));
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public IPAddress IPv4Gate
            {
                get
                {
                    UInt32 val;
                    lpcie.Errs err = E502_EthConfigGetIPv4Gate(cfg_hnd, out val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return uintToIpAddr(val);
                }
                set
                {
                    lpcie.Errs err = E502_EthConfigSetIPv4Gate(cfg_hnd, ipAddrToUint(value));
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public PhysicalAddress UserMac
            {
                get
                {
                    byte[] vals = new byte[MAC_ADDR_SIZE];
                    lpcie.Errs err = E502_EthConfigGetUserMac(cfg_hnd, vals);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return new PhysicalAddress(vals);
                }
                set
                {
                    byte[] vals = value.GetAddressBytes();
                    if (vals.Length < MAC_ADDR_SIZE)
                        throw new Exception(lpcie.Errs.INSUFFICIENT_ARRAY_SIZE);
                    lpcie.Errs err = E502_EthConfigSetUserMac(cfg_hnd, vals);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public PhysicalAddress FactoryMac
            {
                get
                {
                    byte[] vals = new byte[MAC_ADDR_SIZE];
                    lpcie.Errs err = E502_EthConfigGetFactoryMac(cfg_hnd, vals);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return new PhysicalAddress(vals);
                }
            }

            public string InstanceName
            {
                get
                {
                    byte[] val = new byte[INSTANCE_NAME_SIZE];
                    lpcie.Errs err = E502_EthConfigGetInstanceName(cfg_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return System.Text.Encoding.UTF8.GetString(val).Split('\0')[0]; ;
                }
                set
                {
                    lpcie.Errs err = E502_EthConfigSetInstanceName(cfg_hnd, value);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }

            public string NewPassword
            {
                set
                {
                    lpcie.Errs err = E502_EthConfigSetNewPassword(cfg_hnd, value);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                }
            }
        }  
        
        public class EthSvcRecord
        {
            public IntPtr svc_hnd;
            public EthSvcRecord() { svc_hnd = IntPtr.Zero; }
            ~EthSvcRecord() { E502_EthSvcRecordFree(svc_hnd); }

            
            public string InstanceName
            {
                get
                {
                    byte[] val = new byte[INSTANCE_NAME_SIZE];
                    lpcie.Errs err = E502_EthSvcRecordGetInstanceName(svc_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return System.Text.Encoding.UTF8.GetString(val).Split('\0')[0]; ;
                }
            }

            public string DevSerial
            {
                get
                {
                    byte[] val = new byte[SERIAL_SIZE];
                    lpcie.Errs err = E502_EthSvcRecordGetDevSerial(svc_hnd, val);
                    if (err != lpcie.Errs.OK)
                        throw new Exception(err);
                    return Encoding.UTF8.GetString(val).Split('\0')[0];
                }
            }

            public lpcie.Errs ResolveIPv4Addr(out IPAddress addr, UInt32 tout)
            {
                UInt32 addr_wrd;
                lpcie.Errs err = E502_EthSvcRecordResolveIPv4Addr(svc_hnd, out addr_wrd, tout);
                if (err == lpcie.Errs.OK)
                    addr = uintToIpAddr(addr_wrd);
                else
                    addr = null;
                return err;
            }

            public lpcie.Errs ResolveIPv4Addr(out string addr_str, UInt32 tout)
            {
                IPAddress addr;
                lpcie.Errs err = ResolveIPv4Addr(out addr, tout);
                if (err == lpcie.Errs.OK)
                    addr_str = addr.ToString();
                else
                    addr_str = "";
                return err;
            }
        }


        public class EthSvcBrowser
        {
            IntPtr browse_hnd;

            public lpcie.Errs Start()
            {
                return E502_EthSvcBrowseStart(out browse_hnd, 0);
            }

            public lpcie.Errs GetEvent(out EthSvcRecord rec, out EthSvcEvent SvcEvent, UInt32 tout)
            {
                rec = new EthSvcRecord();
                UInt32 flags;
                return E502_EthSvcBrowseGetEvent(browse_hnd, out rec.svc_hnd, out SvcEvent, out flags, tout);
            }

            public lpcie.Errs Stop()
            {
                return E502_EthSvcBrowseStop(browse_hnd);
            }
        }

        static public X502.DevRec MakeDevRecordByIpAddr(EthSvcRecord svc, UInt32 flags, UInt32 tout)
        {
            DevRec.DevRecStruct devstruct;
            lpcie.Errs err = E502_MakeDevRecordByEthSvc(out devstruct, svc.svc_hnd, flags, tout);
            if (err != lpcie.Errs.OK)
                return null;
            X502.DevRec rec = new X502.DevRec(devstruct);
            return rec;
        }
        
  
    }
}
