﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Net.NetworkInformation;
using lpcieapi;

namespace x502api
{
    public class X502
    {
        /* исключение при неверных параметрах */
        public class Exception : System.ApplicationException
        {
            lpcie.Errs m_err;
            public lpcie.Errs ErrorCode { get { return m_err; } }

            public Exception(lpcie.Errs err)
            {
                m_err = err;
            }

            public override string ToString()
            {
                return GetErrorString(m_err);
            }
        }

        /** Максимальное количество логических каналов в таблице*/
        public const UInt32 LTABLE_MAX_CH_CNT = 256;
        /** Количество диапазонов для измерения напряжений */
        public const UInt32 ADC_RANGE_CNT = 6;
        /** Количество каналов АЦП в режиме с общей землей */
        public const UInt32 ADC_COMM_CH_CNT = 32;
        /** Количество каналов АЦП в дифференциальном режиме */
        public const UInt32 ADC_DIFF_CH_CNT = 16;
        /** Максимальное значение для аппаратного усреднения по логическому каналу */
        public const UInt32 LCH_AVG_SIZE_MAX = 128;
        /** Максимальное значения делителя частоты АЦП */
        public const UInt32 ADC_FREQ_DIV_MAX = (1024 * 1024);
        /** Максимальное значение делителя частоты синхронного цифрового ввода */
        public const UInt32 DIN_FREQ_DIV_MAX = (1024 * 1024);
        /** Минимальное значение делителя частоты синхронного вывода */
        public const UInt32 OUT_FREQ_DIV_MIN = 2;
        /** Максимальное значение делителя частоты синхронного вывода */
        public const UInt32 OUT_FREQ_DIV_MAX = 1024;
        /** Значение делителя частоты вывода по умолчанию (которое также всегда
            используется в L502 с версией прошивки ПЛИС ниже 0.5) */
        public const UInt32 X502_OUT_FREQ_DIV_DEFAULT = 2;
        /** Максимальное значение межкадровой задержки для АЦП */
        public const UInt32 ADC_INTERFRAME_DELAY_MAX = (0x1FFFFF);
        /** Таймаут по умолчанию для выполнения команды к BlackFin*/
        public const UInt32 BF_CMD_DEFAULT_TOUT = 500;
        /** Код АЦП, соответствующий максимальному значению шкалы */
        public const UInt32 ADC_SCALE_CODE_MAX = 6000000;
        /** Код ЦАП, соответствующий максимальному значению шкалы */
        public const UInt32 DAC_SCALE_CODE_MAX = 30000;

        /** Максимальное количество символов в строке с названием устройства */
        public const UInt32 DEVNAME_SIZE = 32;
        /** Максимальное количество символов в строке с серийным номером */
        public const UInt32 SERIAL_SIZE = 32;
        /** Максимальное количество символов в строке с описанием подключения */
        public const UInt32 LOCATION_STR_SIZE = 64;
        /** Размер MAC-адреса для Ethernet интерфейса */
        public const UInt32 MAC_ADDR_SIZE  = 6;
        /** Размер строки с описанием экземпляра устройства */
        public const UInt32 INSTANCE_NAME_SIZE =  64;
        /** Максимальный размер строки с паролем на настройки */
        public const UInt32 PASSWORD_SIZE = 32;

        /** Максимально возможное значение внешней опорной частоты */
        public const UInt32 EXT_REF_FREQ_MAX = 1500000;

        /** Размер пользовательской области Flash-памяти */
        public const UInt32 FLASH_USER_SIZE = 0x100000;

        /** Стандартный таймаут на выполнение запроса к BlackFin в мс */
        public const UInt32 BF_REQ_TOUT = 500;


        /** Диапазон ЦАП в вольтах */
        public const UInt32 DAC_RANGE = 5;

        /** Количество каналов ЦАП */
        public const UInt32 DAC_CH_CNT = 2;

        /** Количество цифровых выходов у модуля */
        public const UInt32 X502_DOUT_LINES_CNT = 16;

        /** слово в потоке, означающее, что произошло переполнение */
        public const UInt32 STREAM_IN_MSG_OVERFLOW = 0x01010000;


        /** Интерфейс соединения с модулем */
        public enum Ifaces : byte
        {
            UNKNOWN = 0, /* Неизвестный интерфейс */
            USB = 1, /* Устройство подключено по USB */
            IFACE_ETH = 2, /* Устройство подключено по Ethernet через TCP/IP */
            IFACE_PCI = 3  /* Устройство подключено по PCI/PCIe */
        }


        /** Флаги, управляющие поиском присутствующих модулей */
        [Flags]
        public enum GetDevsFlags : uint
        {
            /** Признак, что нужно вернуть серийные номера только тех устройств,
                которые еще не открыты */
            ONLY_NOT_OPENED = 0x0001
        }

        /** Флаги для управления цифровыми выходами */
        [Flags]
        public enum DigoutFlags : uint
        {
            WORD_DIS_H = 0x00020000, /**< Запрещение (перевод в третье состояние)
                                                 старшей половины цифровых выходов */
            WORD_DIS_L = 0x00010000  /**< Запрещение младшей половины
                                                      цифровых выходов */
        }

        /** Константы для выбора опорной частоты */
        public enum RefFreqVal : uint
        {
            FREQ_2000KHZ = 2000000, /**< Частота 2МГц */
            FREQ_1500KHZ = 1500000 /**< Частота 1.5МГц */
        }

        /** Диапазоны измерения для канала АЦП */
        public enum AdcRange : uint
        {
            RANGE_10 = 0, /**< Диапазон +/-10V */
            RANGE_5 = 1, /**< Диапазон +/-5V */
            RANGE_2 = 2, /**< Диапазон +/-2V */
            RANGE_1 = 3, /**< Диапазон +/-1V */
            RANGE_05 = 4, /**< Диапазон +/-0.5V */
            RANGE_02 = 5  /**< Диапазон +/-0.2V */
        }

        /** Режим измерения для логического канала */
        public enum LchMode : uint
        {
            COMM = 0, /**< Измерение напряжения относительно общей земли */
            DIFF = 1, /**< Дифференциальное измерение напряжения */
            ZERO = 2  /**< Измерение собственного нуля */
        }

        /** Режимы синхронизации */
        public enum Sync : uint
        {
            INTERNAL = 0, /**< Внутренний сигнал */
            EXTERNAL_MASTER = 1, /**< От внешнего мастера по разъему синхронизации */
            DI_SYN1_RISE = 2, /**< По фронту сигнала DI_SYN1 */
            DI_SYN1_FALL = 3, /**< По спаду сигнала DI_SYN1 */
            DI_SYN2_RISE = 6, /**< По фронту сигнала DI_SYN2 */
            DI_SYN2_FALL = 7  /**< По спаду сигнала DI_SYN2 */
        }

        /** Флаги, управляющие обработкой принятых данных */
        [Flags]
        public enum ProcFlags : uint
        {
            /* Признак, что нужно преобразовать значения АЦП в вольты */
            VOLT = 1,
            /** Признак, что не нужно проверять совпадение номеров каналов
                в принятых данных с каналами из логической таблицы.
                Может использоваться при нестандартной прошивке BlackFin
                при передаче в ПК не всех данных. */
            DONT_CHECK_CH   = 0x00010000
        }


        /** Флаги для обозначения синхронных потоков данных */
        [Flags]
        public enum Streams : uint
        {
            ADC = 0x01, /**< Поток данных от АЦП */
            DIN = 0x02, /**< Поток данных с цифровых входов */
            DAC1 = 0x10, /**< Поток данных первого канала ЦАП */
            DAC2 = 0x20, /**< Поток данных второго канала ЦАП */
            DOUT = 0x40, /**< Поток данных на цифровые выводы */
            /** Объединение всех флагов, обозначающих потоки данных на ввод */
            ALL_IN = ADC | DIN,
            /** Объединение всех флагов, обозначающих потоки данных на вывод */
            ALL_OUT = DAC1 | DAC2 | DOUT
        }

        /** Константы, определяющие тип передаваемого отсчета из ПК в модуль */
        public enum StreamOutWordType : uint
        {
            DOUT = 0x0, /**< Цифровой вывод */
            DAC1 = 0x40000000, /**< Код для 1-го канала ЦАП */
            DAC2 = 0x80000000  /**< Код для 2-го канала ЦАП */
        };

        /** Режим работы модуля L502 */
        public enum Mode : uint
        {
            FPGA = 0, /**< Все потоки данных передаются через ПЛИС минуя
                              сигнальный процессор BlackFin */
            DSP = 1, /**< Все потоки данных передаются через сигнальный
                              процессор, который должен быть загружен
                              прошивкой для обработки этих потоков */
            DEBUG = 2  /**< Отладочный режим */
        }

        /** Номера каналов ЦАП. */
        public enum DacCh : uint
        {
            CH1 = 0, /**< Первый канал ЦАП */
            CH2 = 1  /**< Второй канал ЦАП */
        }

        /** Флаги, используемые при выводе данных на ЦАП. */
        [Flags]
        public enum DacOutFlags : uint
        {
            /** Указывает, что значение задано в Вольтах и при выводе его нужно
                перевести его в коды ЦАП. Если флаг не указан, то считается, что значение
                изначально в кодах */
            VOLT = 0x0001,
            /** Указывает, что нужно применить калибровочные коэффициенты перед
                выводом значения на ЦАП. */
            CALIBR = 0x0002
        }
        
    /** Номера каналов для передачи потоков данных */
        public enum StreamCh : uint
        {
            IN = 0, /**< Общий канал на ввод */
            OUT = 1  /**< Общий канал на вывод */
        };

        /**  Цифровые линии, на которых можно включить подтягивающие резисторы */
        [Flags]
        public enum Pullups : uint
        {
            DI_H = 0x01, /**< Старшая половина цифровых входов (только L502) */
            DI_L = 0x02, /**< Младшая половина цифровых входов (только L502) */
            DI_SYN1 = 0x04, /**< Линия SYN1 */
            DI_SYN2 = 0x08,  /**< Линия SYN2 */
            CONV_IN  = 0x10, /**< Подтяжка к 0 линии межмодульной
                                  синхронизации CONV_IN (только для E502) */
            START_IN = 0x20, /**< Подтяжка к 0 линии межмодульной
                                  синхронизации START_IN (только для E502) */
        }


        /** Флаги, определяющие наличие опций в модуле */
        [Flags]
        public enum DevFlags : uint
        {
            /** Признак наличия двухканального канального ЦАП */
            DAC_PRESENT = 0x00000001,
            /** Признак наличия гальваноразвязки */
            GAL_PRESENT = 0x00000002,
            /** Признак наличия сигнального процессора BlackFin */
            BF_PRESENT = 0x00000004,
            /** Признак, что устройство поддерживает интерфейс USB */
            IFACE_SUPPORT_USB = 0x00000100,
            /** Признак, что устройство поддерживает Ethernet */
            IFACE_SUPPORT_ETH = 0x00000200,
            /** Признак, что устройство поддерживает интерфейс PCI/PCI-Express */
            IFACE_SUPPORT_PCI = 0x00000400,

            /** Признак, что устройство выполнено в индустриалном исполнении */
            INDUSTRIAL = 0x00008000,
            /** Признак, что во Flash-памяти присутствует информация о модуле */
            FLASH_DATA_VALID = 0x00010000,
            /** Признак, что во Flash-памяти присутствуют действительные калибровочные
                коэффициенты АЦП */
            FLASH_ADC_CALIBR_VALID = 0x00020000,
            /** Признак, что во Flash-памяти присутствуют действительные калибровочные
                коэффициенты ЦАП */
            FLASH_DAC_CALIBR_VALID = 0x00040000,
            /** Признак, что присутствует прошивка ПЛИС и она успешно была загружена */
            FPGA_LOADED = 0x00800000,
            /** Признак, что устройство уже открыто (действителен только внутри t_x502_devrec) */
            DEVREC_OPENED = 0x01000000
        }

        public enum LocationTypes : byte 
        {
            NONE          = 0, /** В поле расположения устройства не содержится информации */
            ADDR          = 1, /** В поле расположения устройства содержится строка с адресом устройства */
            INSTANCE_NAME = 2  /** В поле ресположения устройства содержится строка с именем экземпляра */
        }

        [Flags]
        public enum OutCycleFlags : uint
        {
            FORCE = 0x01,
            WAIT_DONE = 0x02
        }

        public enum Feature : uint {
            OUT_FREQ_DIV       = 1,
            OUT_STATUS_FLAGS   = 2
        }

        /** Флаги состояния для синхронного вывода */
        [Flags]
        public enum OutStatusFlags {
            /** Флаг указывает, что в настоящее время буфер в модуле на передачу пуст */
            BUF_IS_EMPTY = 0x01,
            /** Флаг указывает, что было опустошение буфера на вывод с начала старта синхронного
                ввода-вывода или с момента последнего чтения статуса с помощью
                X502_OutGetStatusFlags() (в зависимости от того, что было последним) */
            BUF_WAS_EMPTY = 0x02
        }




        public class DevRec
        {
            /** Структура, описывающая устройство, по которой с ним можно установить соединение */
            [StructLayout(LayoutKind.Sequential)]
            public struct DevRecStruct
            {
                UInt32 sign_;
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)DEVNAME_SIZE)]
                char[] devname_; /**< Название устройства */
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)SERIAL_SIZE)]
                char[] serial_; /**< Серийный номер */
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)LOCATION_STR_SIZE)]
                char[] location_; /**< Описание подключения (если есть) */
                DevFlags flags_; /**< Флаги из #t_x502_dev_flags, описывающие устройство */
                Ifaces iface_; /**< Интерфейс, по которому подключено устройство */
                LocationTypes locationType_; /**< Определяет, что именно сохранено в поле location */
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = 122)]
                byte[] res;  /**< Резерв */
                IntPtr internal_; /**< Непрозрачный указатель на структуру с
                                       информацией об устройстве для возможности
                                       его открытия. */

                public string DevName { get { return new string(devname_).Split('\0')[0]; } }
                public string Serial { get { return new string(serial_).Split('\0')[0]; } }
                public string Location { get { return new string(location_).Split('\0')[0]; } }
                public DevFlags Flags { get { return flags_; } }
                public Ifaces Iface { get { return iface_; } }
                public LocationTypes LocationType { get { return locationType_; } }
            }


            public DevRecStruct intStruct;


            public string DevName { get { return intStruct.DevName; } }
            public string Serial { get { return intStruct.Serial;  } }
            public string Location { get { return intStruct.Location; } }
            public DevFlags Flags { get { return intStruct.Flags; } }
            public Ifaces Iface { get { return intStruct.Iface; } }
            public LocationTypes LocationType { get { return intStruct.LocationType; } }
            
            public DevRec(DevRecStruct devrec)
            {
                intStruct = devrec;
            }
            ~DevRec()
            {
                X502_FreeDevRecordList(ref intStruct, 1);
            }
        }


        /** Калибровочные коэффициенты диапазона. */
        [StructLayout(LayoutKind.Sequential)]
        public struct CbrCoef
        {
            double _offs; /**< смещение нуля */
            double _k; /**< коэффициент шкалы */
            public double Offs { get { return _offs; } }
            public double k { get { return _k; } }
        };


        /**  Калибровочные коэффициенты модуля. */
        [StructLayout(LayoutKind.Sequential)]
        public struct Cbr
        {
            /** Калибровочные коэффициенты АЦП */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)ADC_RANGE_CNT)]
            CbrCoef[] adc;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            UInt32[] res1; /**< Резерв */
            /** Калибровочные коэффициенты ЦАП */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)DAC_CH_CNT)]
            CbrCoef[] dac;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            UInt32[] res2; /**< Резерв */

            public CbrCoef[] Adc { get { return adc; } }
            public CbrCoef[] Dac { get { return dac; } }
        };

        /** Информация о модуле L502. */
        [StructLayout(LayoutKind.Sequential)]
        public struct Info
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)DEVNAME_SIZE)]
            char[] name_; /**< Название устройства ("L502") */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)SERIAL_SIZE)]
            char[] serial_; /**< Серийный номер */
            DevFlags devflags_; /**< Флаги из #t_dev_flags, описывающие наличие
                            в модуле определенных опций */
            UInt16 fpga_ver_; /**< Версия ПЛИС (старший байт - мажорная, младший - минорная) */
            Byte plda_ver_; /**< Версия ПЛИС, управляющего аналоговой частью */
            Byte board_rev_; /**< Ревизия платы */
            UInt32 mcu_firmware_ver_; /**< Версия прошивки контроллера Cortex-M4. Действительна только для E502 */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)MAC_ADDR_SIZE)]
            byte[]  factory_mac; /**< Заводской MAC-адрес --- действителен только для
                                      устройств с Ethernet-интерфейсом */
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 110)]
            Byte[] res; /**< Резерв */
            Cbr cbr_; /**< Заводские калибровочные коэффициенты (из Flash-памяти) */

            public string Name { get { return new string(name_).Split('\0')[0]; } }
            public string Serial { get { return new string(serial_).Split('\0')[0]; } }
            public DevFlags DevFlags { get { return devflags_; } }
            public UInt16 FpgaVer { get { return fpga_ver_; } }
            public string FpgaVerString { get { return String.Format("{0}.{1}", (fpga_ver_ >> 8) & 0xFF, fpga_ver_ & 0xFF); } }
            public Byte PldaVer { get { return plda_ver_; } }
            public Byte BoardRevision { get { return board_rev_; } }
            public UInt32 McuFirmwareVer { get { return mcu_firmware_ver_; } }
            public string McuFirmwareVerString
            {
                get
                {
                    return mcu_firmware_ver_ == 0 ? "" : String.Format("{0}.{1}.{2}.{3}", (mcu_firmware_ver_ >> 24) & 0xFF,
                        (mcu_firmware_ver_ >> 16) & 0xFF, (mcu_firmware_ver_ >> 8) & 0xFF, mcu_firmware_ver_ & 0xFF);
                }
            }
            public PhysicalAddress FactoryMac { get { return new PhysicalAddress(factory_mac); } }
            public Cbr Cbr { get { return cbr_; } }
        };




        /* класс - описание логического канала */
        public class LChannel
        {
            UInt32 m_avg;
            UInt32 m_phy_ch;
            LchMode m_mode;
            AdcRange m_range;

            public LChannel(UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg)
            {
                m_phy_ch = phy_ch;
                m_mode = mode;
                m_range = range;
                m_avg = avg;
            }

            public LchMode Mode { get { return m_mode; } }
            public AdcRange Range { get { return m_range; } }
            public UInt32 PhyCh { get { return m_phy_ch; } }
            public UInt32 Avg { get { return m_avg; } }
        }

        /********************* приватные обертки для исходных функций **************/
        [DllImport("x502api.dll")]
        static extern IntPtr X502_Create();
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_Free(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OpenByDevRecord(IntPtr hnd, ref DevRec.DevRecStruct devrec);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FreeDevRecordList(ref DevRec.DevRecStruct devrec, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_IsOpened(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_Close(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetDevInfo(IntPtr hnd, out Info info);


        /* Функции для изменения настроек модуля */
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_Configure(IntPtr hnd, UInt32 flags);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetLChannel(IntPtr hnd, UInt32 lch, UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetLChannelCount(IntPtr hnd, UInt32 lch_cnt);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetLChannelCount(IntPtr hnd, out UInt32 lch_cnt);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetAdcFreqDivider(IntPtr hnd, UInt32 adc_freq_div);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetAdcInterframeDelay(IntPtr hnd, UInt32 delay);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetDinFreqDivider(IntPtr hnd, UInt32 din_freq_div);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetOutFreqDivider(IntPtr hnd, UInt32 out_freq_div);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetAdcFreq(IntPtr hnd, ref double f_adc, ref double f_frame);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetDinFreq(IntPtr hnd, ref double f_din);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetOutFreq(IntPtr hnd, ref double f_dout);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetAdcFreq(IntPtr hnd, out double f_adc, out double f_frame);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetRefFreq(IntPtr hnd, RefFreqVal freq);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetExtRefFreqValue(IntPtr hnd, double freq);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetRefFreqValue(IntPtr hnd, out double freq);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetSyncMode(IntPtr hnd, Sync sync_mode);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetSyncStartMode(IntPtr hnd, Sync sync_start_mode);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetMode(IntPtr hnd, Mode mode);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetMode(IntPtr hnd, out Mode mode);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetAdcCoef(IntPtr hnd, AdcRange range, double k, double offs);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetAdcCoef(IntPtr hnd, AdcRange range, out double k, out double offs);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetDacCoef(IntPtr hnd, DacCh ch, double k, double offs);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetDacCoef(IntPtr hnd, DacCh ch, out double k, out double offs);
        /* Функции асинхронного ввода-вывода */
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_AsyncOutDac(IntPtr hnd, DacCh ch, double data, DacOutFlags flags);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_AsyncOutDig(IntPtr hnd, UInt32 val, UInt32 msk);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_AsyncInDig(IntPtr hnd, out UInt32 din);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_AsyncGetAdcFrame(IntPtr hnd, ProcFlags flags, UInt32 tout, double[] data);
        /* Функции для работы с синхронным потоковым вводом-выводом */
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_StreamsEnable(IntPtr hnd, Streams streams);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_StreamsDisable(IntPtr hnd, Streams streams);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetEnabledStreams(IntPtr hnd, out Streams streams);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_StreamsStart(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_IsRunning(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_StreamsStop(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern Int32 X502_Recv(IntPtr hnd, UInt32[] buf, UInt32 size, UInt32 tout);
        [DllImport("x502api.dll")]
        static extern Int32 X502_Send(IntPtr hnd, UInt32[] buf, UInt32 size, UInt32 tout);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_ProcessAdcData(IntPtr hnd, UInt32[] src, double[] dest, ref UInt32 size, ProcFlags flags);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_ProcessData(IntPtr hnd, UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_ProcessDataWithUserExt(IntPtr hnd, UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size,
            UInt32[] usr_data, ref UInt32 usr_data_size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_PrepareData(IntPtr hnd, double[] dac1, double[] dac2, UInt32[] digout,
            UInt32 size, DacOutFlags flags, UInt32[] out_buf);

        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetRecvReadyCount(IntPtr hnd, out UInt32 rdy_cnt);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetSendReadyCount(IntPtr hnd, out UInt32 rdy_cnt);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_GetNextExpectedLchNum(IntPtr hnd, out UInt32 lch);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_PreloadStart(IntPtr hnd);

        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OutCycleLoadStart(IntPtr hnd, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OutCycleSetup(IntPtr hnd, OutCycleFlags flags);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OutCycleStop(IntPtr hnd, OutCycleFlags flags);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OutCycleCheckSetupDone(IntPtr hnd, out bool done);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_OutGetStatusFlags(IntPtr hnd, out OutStatusFlags status);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetStreamBufSize(IntPtr hnd, StreamCh ch, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetStreamStep(IntPtr hnd, StreamCh ch, UInt32 step);
        //Функции для работы с сигнальным процессором
        [DllImport("x502api.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        static extern lpcie.Errs X502_BfLoadFirmware(IntPtr hnd, string filename);

        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_BfCheckFirmwareIsLoaded(IntPtr hnd, out UInt32 version);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_BfMemRead(IntPtr hnd, UInt32 addr, UInt32[] regs, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_BfMemWrite(IntPtr hnd, UInt32 addr, UInt32[] regs, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_BfExecCmd(IntPtr hnd, UInt16 cmd_code, UInt32 par,
            UInt32[] snd_data, UInt32 snd_size, UInt32[] rcv_data, UInt32 rcv_size, UInt32 tout, out UInt32 recvd_size);

        //Функции для работы с Flash-памятью модуля
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FlashRead(IntPtr hnd, UInt32 addr, byte[] data, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FlashWrite(IntPtr hnd, UInt32 addr, byte[] data, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FlashErase(IntPtr hnd, UInt32 addr, UInt32 size);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FlashWriteEnable(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_FlashWriteDisable(IntPtr hnd);


        //Дополнительные вспомогательные функции
        [DllImport("x502api.dll", EntryPoint = "X502_GetLibraryVersion")]
        public static extern UInt32 GetLibraryVersion();
        [DllImport("x502api.dll", CharSet = CharSet.Ansi)]
        static extern IntPtr X502_GetErrorString(lpcie.Errs err);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_LedBlink(IntPtr hnd);
        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_SetDigInPullup(IntPtr hnd, Pullups pullups);

        [DllImport("x502api.dll")]
        static extern lpcie.Errs X502_CheckFeature(IntPtr hnd, Feature feature);

        /**********************************  методы класса **************/
        public X502()
        {
            hnd = X502_Create();                            
        }

        ~X502()
        {
            X502_Free(hnd);
        }

        static public X502 Create(string devname) {
            if (devname == "L502")
                return new L502();
            if (devname == "E502")
                return new E502();
            return null;
        }

        public lpcie.Errs Open(DevRec rec)
        {
            return X502_OpenByDevRecord(hnd, ref rec.intStruct);
        }

        public lpcie.Errs Close() { return X502_Close(hnd); }
        
        public bool IsOpened()
        {
            return X502_IsOpened(hnd) == lpcie.Errs.OK;
        }

        public Info DevInfo
        {
            get
            {
                Info info;
                lpcie.Errs res = X502_GetDevInfo(hnd, out info);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return info;
            }
        }

        public lpcie.Errs Configure(UInt32 flags)
        {
            return X502_Configure(hnd, flags);
        }

        public lpcie.Errs SetLChannel(UInt32 lch, UInt32 phy_ch, LchMode mode, AdcRange range, UInt32 avg)
        {
            return X502_SetLChannel(hnd, lch, phy_ch, mode, range, avg);
        }

        public LChannel[] LChannelTable
        {
            set
            {
                for (UInt32 i = 0; (i < value.Length) && (i < LTABLE_MAX_CH_CNT); i++)
                {
                    lpcie.Errs res = SetLChannel(i, value[i].PhyCh, value[i].Mode, value[i].Range, value[i].Avg);
                    if (res != lpcie.Errs.OK)
                        throw new Exception(res);
                }
            }
        }

        public UInt32 LChannelCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs res = X502_GetLChannelCount(hnd, out val);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return val;
            }
            set
            {
                lpcie.Errs res = X502_SetLChannelCount(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 AdcFreqDivider
        {
            set
            {
                lpcie.Errs res = X502_SetAdcFreqDivider(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 AdcInterframeDelay
        {
            set
            {
                lpcie.Errs res = X502_SetAdcInterframeDelay(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 DinFreqDivider
        {
            set
            {
                lpcie.Errs res = X502_SetDinFreqDivider(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public UInt32 OutFreqDivider
        {
            set
            {
                lpcie.Errs res = X502_SetOutFreqDivider(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public lpcie.Errs SetAdcFreq(ref double f_adc, ref double f_frame)
        {
            return X502_SetAdcFreq(hnd, ref f_adc, ref f_frame);
        }

        public lpcie.Errs SetDinFreq(ref double f_din)
        {
            return X502_SetDinFreq(hnd, ref f_din);
        }
        public lpcie.Errs SetOutFreq(ref double f_out)
        {
            return X502_SetOutFreq(hnd, ref f_out);
        }

        public lpcie.Errs GetAdcFreq(out double f_adc, out double f_frame)
        {
            return X502_GetAdcFreq(hnd, out f_adc, out f_frame);
        }

        public RefFreqVal RefFreq
        {
            set
            {
                lpcie.Errs res = X502_SetRefFreq(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }            
        }

        public double ExtRefFreqValue
        {
            set
            {
                lpcie.Errs res = X502_SetExtRefFreqValue(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }
        public double RefFreqValue
        {
            get
            {
                double freq;
                lpcie.Errs res = X502_GetRefFreqValue(hnd, out freq);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return freq;
            }
        }

        public Sync SyncMode
        {
            set
            {
                lpcie.Errs res = X502_SetSyncMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public Sync SyncStartMode
        {
            set
            {
                lpcie.Errs res = X502_SetSyncStartMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }

        public Mode ModuleMode
        {
            get
            {
                Mode mode;
                lpcie.Errs res = X502_GetMode(hnd, out mode);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
                return mode;

            }
            set
            {
                lpcie.Errs res = X502_SetMode(hnd, value);
                if (res != lpcie.Errs.OK)
                    throw new Exception(res);
            }
        }


        public lpcie.Errs SetAdcCoef(AdcRange range, double k, double offs)
        {
            return X502_SetAdcCoef(hnd, range, k, offs);
        }

        public lpcie.Errs GetAdcCoef(AdcRange range, out double k, out double offs)
        {
            return X502_GetAdcCoef(hnd, range, out k, out offs);
        }

        public lpcie.Errs SetDacCoef(DacCh ch, double k, double offs)
        {
            return X502_SetDacCoef(hnd, ch, k, offs);
        }

        public lpcie.Errs GetDacCoef(DacCh ch, out double k, out double offs)
        {
            return X502_GetDacCoef(hnd, ch, out k, out offs);
        }

        public lpcie.Errs AsyncOutDac(DacCh ch, double data, DacOutFlags flags)
        {
            return X502_AsyncOutDac(hnd, ch, data, flags);
        }
        public lpcie.Errs AsyncOutDig(UInt32 val, UInt32 msk)
        {
            return X502_AsyncOutDig(hnd, val, msk);
        }
        public lpcie.Errs AsyncInDig(out UInt32 din)
        {
            return X502_AsyncInDig(hnd, out din);
        }
        public lpcie.Errs AsyncGetAdcFrame(ProcFlags flags, UInt32 tout, double[] data)
        {
            return data.Length < LChannelCount ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                   X502_AsyncGetAdcFrame(hnd, flags, tout, data);
        }

        public lpcie.Errs StreamsEnable(Streams streams)
        {
            return X502_StreamsEnable(hnd, streams);
        }
        public lpcie.Errs StreamsDisable(Streams streams)
        {
            return X502_StreamsDisable(hnd, streams);
        }

        public Streams EnabledStreams
        {
            get
            {
                Streams streams;
                lpcie.Errs err = X502_GetEnabledStreams(hnd, out streams);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return streams;
            }
        }


        public lpcie.Errs StreamsStart()
        {
            return X502_StreamsStart(hnd);
        }
        public lpcie.Errs StreamsStop()
        {
            return X502_StreamsStop(hnd);
        }
        public bool IsRunning()
        {
            return X502_IsRunning(hnd) == lpcie.Errs.OK;
        }

        public Int32 Recv(UInt32[] buf, UInt32 size, UInt32 tout)
        {
            return buf.Length < size ? (Int32)lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_Recv(hnd, buf, size, tout);
        }

        public Int32 Send(UInt32[] buf, UInt32 size, UInt32 tout)
        {
            return buf.Length < size ? (Int32)lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_Send(hnd, buf, size, tout);
        }

        public lpcie.Errs ProcessAdcData(UInt32[] src, double[] dest, ref UInt32 size, ProcFlags flags)
        {
            return (src.Length < size) || (dest.Length < size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_ProcessAdcData(hnd, src, dest, ref size, flags);
        }

        public lpcie.Errs ProcessData(UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size)
        {
            return (src.Length < size) || (adc_data.Length < adc_data_size) || (din_data.Length < din_data_size) ?
                lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_ProcessData(hnd, src, size, flags, adc_data, ref adc_data_size, din_data, ref din_data_size);
        }

        public lpcie.Errs ProcessDataWithUserExt(UInt32[] src, UInt32 size, ProcFlags flags,
            double[] adc_data, ref UInt32 adc_data_size, UInt32[] din_data, ref UInt32 din_data_size,
            UInt32[] usr_data, ref UInt32 usr_data_size)
        {
            return (src.Length < size) || (adc_data.Length < adc_data_size) || (din_data.Length < din_data_size)
                || (usr_data.Length < usr_data_size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
               X502_ProcessDataWithUserExt(hnd, src, size, flags,
                adc_data, ref adc_data_size, din_data, ref din_data_size,
                usr_data, ref usr_data_size);
        }

        public lpcie.Errs PrepareData(double[] dac1, double[] dac2, UInt32[] digout,
            UInt32 size, DacOutFlags flags, UInt32[] out_buf)
        {
            return X502_PrepareData(hnd, dac1, dac2, digout, size, flags, out_buf);
        }

        public UInt32 RecvReadyCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = X502_GetRecvReadyCount(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public UInt32 SendReadyCount
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = X502_GetSendReadyCount(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public UInt32 NextExpectedLchNum
        {
            get
            {
                UInt32 val;
                lpcie.Errs err = X502_GetNextExpectedLchNum(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        public lpcie.Errs PreloadStart()
        {
            return X502_PreloadStart(hnd);
        }

        public lpcie.Errs OutCycleLoadStart(UInt32 size)
        {
            return X502_OutCycleLoadStart(hnd, size);
        }

        public lpcie.Errs OutCycleSetup(OutCycleFlags flags)
        {
            return X502_OutCycleSetup(hnd, flags);
        }

        public lpcie.Errs OutCycleStop(OutCycleFlags flags)
        {
            return X502_OutCycleStop(hnd, flags);
        }

        public lpcie.Errs OutCycleCheckSetupDone(out bool done)
        {
            return X502_OutCycleCheckSetupDone(hnd, out done);
        }

        bool OutCycleSetupDone
        {
            get
            {
                bool val;
                lpcie.Errs err = X502_OutCycleCheckSetupDone(hnd, out val);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return val;
            }
        }

        OutStatusFlags OutStatus
        {
            get 
            {
                OutStatusFlags status;
                lpcie.Errs err = OutGetStatusFlags(out status);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
                return status;
            }
        }

        lpcie.Errs OutGetStatusFlags(out OutStatusFlags status)
        {
            return X502_OutGetStatusFlags(hnd, out status);
        }


        public lpcie.Errs SetStreamBufSize(StreamCh ch, UInt32 size)
        {
            return X502_SetStreamBufSize(hnd, ch, size);
        }

        public lpcie.Errs SetStreamStep(StreamCh ch, UInt32 step)
        {
            return X502_SetStreamStep(hnd, ch, step);
        }

        public lpcie.Errs BfLoadFirmware(string filename)
        {
            return X502_BfLoadFirmware(hnd, filename);
        }
        public lpcie.Errs BfCheckFirmwareIsLoaded(out UInt32 version)
        {
            return X502_BfCheckFirmwareIsLoaded(hnd, out version);
        }
        public lpcie.Errs BfMemRead(UInt32 addr, UInt32[] regs, UInt32 size)
        {
            return regs.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_BfMemRead(hnd, addr, regs, size);
        }
        public lpcie.Errs BfMemWrite(UInt32 addr, UInt32[] regs, UInt32 size)
        {
            return regs.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_BfMemWrite(hnd, addr, regs, size);
        }
        public lpcie.Errs BfExecCmd(UInt16 cmd_code, UInt32 par,
            UInt32[] snd_data, UInt32 snd_size, UInt32[] rcv_data, UInt32 rcv_size, UInt32 tout, out UInt32 recvd_size)
        {
            recvd_size = 0;
            return (snd_data.Length < snd_size) || (rcv_data.Length < rcv_size) ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_BfExecCmd(hnd, cmd_code, par, snd_data, snd_size, rcv_data, rcv_size, tout, out recvd_size);
        }

        public lpcie.Errs FlashRead(UInt32 addr, byte[] data, UInt32 size)
        {
            return data.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_FlashRead(hnd, addr, data, size);
        }
        public lpcie.Errs FlashWrite(UInt32 addr, byte[] data, UInt32 size)
        {
            return data.Length < size ? lpcie.Errs.INSUFFICIENT_ARRAY_SIZE :
                X502_FlashWrite(hnd, addr, data, size);
        }

        public lpcie.Errs FlashErase(UInt32 addr, UInt32 size)
        {
            return X502_FlashErase(hnd, addr, size);
        }
        public lpcie.Errs FlashWriteEnable()
        {
            return X502_FlashWriteEnable(hnd);
        }
        public lpcie.Errs FlashWriteDisable()
        {
            return X502_FlashWriteDisable(hnd);
        }

        
        public static string GetErrorString(lpcie.Errs err)
        {
            IntPtr ptr = X502_GetErrorString(err);
            string str = Marshal.PtrToStringAnsi(ptr);
            Encoding srcEncodingFormat = Encoding.GetEncoding("windows-1251");
            Encoding dstEncodingFormat = Encoding.UTF8;
            return dstEncodingFormat.GetString(Encoding.Convert(srcEncodingFormat, dstEncodingFormat, srcEncodingFormat.GetBytes(str)));
        }

        public lpcie.Errs LedBlink()
        {
            return X502_LedBlink(hnd);
        }

        public Pullups DigInPullup
        {
            set
            {
                lpcie.Errs err = X502_SetDigInPullup(hnd, value);
                if (err != lpcie.Errs.OK)
                    throw new Exception(err);
            }
        }

        public lpcie.Errs CheckFeature(Feature feature)
        {
            return X502_CheckFeature(hnd, feature);
        }

        protected IntPtr hnd; /* описатель модуля */ 
    }
}
